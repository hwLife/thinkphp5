<?php
namespace app\api\controller;

use app\api\service\AdminuserService;
use think\Controller;
use think\Request;

class AdminuserController extends Controller{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getAllUsers(){

         return AdminuserService::getAllUsers();
    }

    public function setId(){
       // return 'hh';
        return "hello ".$this->request->param('id')."!";
    }
    public function getUserById(){
        //$tmp = $this->request->param('id');
        //return $tmp;
        return "hello world";
       //return AdminuserService::getUserById($tmp);
    }

    public function getUserById2(){
        $tmp = $this->request->param('id');

        return AdminuserService::getUserById2($tmp);
    }
}
