<?php

use think\Env;

return [
	'app_status'             => Env::get('status','dev'),
	//'app_status' => 'hehehehh',
	'app_debug'              => 'true',
	'url_route_on'           => true,
	// 是否强制使用路由
    'url_route_must'         => false,
    // 默认输出类型
    'default_return_type'    => 'html',
    // 默认AJAX 数据返回格式,可选json xml ...
    'default_ajax_return'    => 'json',
    // 默认JSONP格式返回的处理方法
    'default_jsonp_handler'  => 'jsonpReturn',
    // 默认JSONP处理方法
    'var_jsonp_handler'      => 'callback',
    // 默认时区
    'default_timezone'       => 'PRC',
];